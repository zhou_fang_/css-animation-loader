/*
 * @Author: Zhou Fang
 * @Date: 2018-01-22 17:55:26
 * @Last Modified by: Zhou Fang
 * @Last Modified time: 2018-01-23 15:21:54
 */

/**
 * @description add logo dot loading animation
 * @class LogoDotAnimation
 * @parameter  parentContainer: HTMLElement
 * @method  show(); hide(); destroy();
 */
class LogoDotAnimation {
    /**
     * @description
     * @type {string}
     * @memberof LogoDotAnimation
     */
    template: string = `
  
    <div class="spinner ">
        <div class="ball ">
            <?xml version="1.0 " encoding="utf-8 "?>
            <!-- Generator: Adobe Illustrator 19.2.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
            <svg version="1.1 "
                 fill="#03A9F4 "
                 xmlns="http://www.w3.org/2000/svg "
                 xmlns:xlink="http://www.w3.org/1999/xlink
    "
                 x="0px "
                 y="0px "
                 viewBox="0 0 50 50 "
                 style="enable-background:new 0 0 50 50; "
                 xml:space="preserve ">
                <g>
                    <path d="M35.2,13.8v22.1h-7.8v-3.4c-1.8,2.6-4.1,3.8-6.9,3.8c-3.8,0-5.7-1.8-5.7-5.5V14.3c2.6-0.1,5.2-0.3,7.8-0.6v16.7 c0,1.5,0.4,2.2,1.3,2.2c2.3,0,3.5-2.5,3.5-7.5V14.3C30,14.2,32.6,14,35.2,13.8z " />
                </g>
            </svg>
        </div>
        <div>LOADING</div>
    </div>

    `;
    css: string = `.l-d-l-con {
      position: relative;
      height: 100%;
      width: 100%;
      display: -ms-flexbox;
      display: flex;
      -ms-flex-pack: center;
          justify-content: center;
      -ms-flex-align: center;
          align-items: center;
      text-align: center;
    }
    .l-d-l-con .spinner {
      height: 3.5rem;
      width: 9rem;
      text-align: center;
      background-color: rgba(0, 0, 0, 0.5);
      padding: 1rem 1.5rem;
      border-radius: 1rem;
    }
    .l-d-l-con .spinner .ball {
      width: 1.25rem;
      height: 1.25rem;
      background-color: #fff;
      border-radius: 50%;
      display: inline-block;
      animation: motion 3s cubic-bezier(0.77, 0, 0.175, 1) infinite;
    }
    .l-d-l-con .spinner div {
      color: rgba(255, 255, 255, 0.8);
      margin-top: 0.625rem;
      font-family: sans-serif;
      letter-spacing: 0.2rem;
      font-size: 0.725rem;
    }
    @keyframes motion {
      0% {
        transform: translateX(0) scale(1);
        opacity: 1;
      }
      25% {
        transform: translateX(-50px) scale(0.6);
        opacity: 0.2;
      }
      50% {
        transform: translateX(0) scale(1);
        opacity: 1;
      }
      75% {
        transform: translateX(50px) scale(0.6);
        opacity: 0.2;
      }
      100% {
        transform: translateX(0) scale(1);
        opacity: 1;
      }
    }     
    `;

    styleNode: HTMLStyleElement;
    htmlNode: HTMLElement;
    head = document.head || document.getElementsByTagName('head')[0];

    /**
     * Creates an instance of LogoDotAnimation.
     * @param {HTMLElement} parentContainer
     * @memberof LogoDotAnimation
     */
    constructor(private parentContainer: HTMLElement) {
        if (!parentContainer || !(parentContainer instanceof HTMLElement))
            throw new Error('Container div error.');

        // append the styles ---
        let cssNode = document.createTextNode(this.css);
        this.styleNode = document.createElement('style');
        this.styleNode.type = 'text/css';
        if (this.styleNode.sheet && this.styleNode.sheet.cssText) {
            this.styleNode.sheet.cssText = cssNode.nodeValue; //ie only
        } else {
            this.styleNode.appendChild(cssNode);
        }
        this.head.appendChild(this.styleNode);

        // append html-----------
        this.htmlNode = document.createElement('div');
        this.htmlNode.className = 'l-d-l-con';
        this.htmlNode.innerHTML = this.template;
        parentContainer.appendChild(this.htmlNode);
    }
    /**
     * @description hide the loading animation html node
     * @memberof LogoDotAnimation
     */
    hide() {
        if (this.htmlNode && this.htmlNode instanceof HTMLElement) {
            this.htmlNode.style.display = 'None';
        } else {
            throw new Error('loading element not exit');
        }
    }
    /**
     * @description show the loading animation html node.
     * @memberof LogoDotAnimation
     */
    show() {
        if (this.htmlNode && this.htmlNode instanceof HTMLElement) {
            this.htmlNode.style.display = '';
        } else {
            throw new Error('loading style element not exit');
        }
    }
    /**
     * @description remove the loading animation html node and the stylesheet node
     * @memberof LogoDotAnimation
     */
    destroy() {
        if (this.htmlNode && this.htmlNode instanceof HTMLElement) {
            this.htmlNode.remove();
        }
        if (this.styleNode && this.styleNode instanceof HTMLStyleElement) {
            this.styleNode.remove();
        }
    }
}
