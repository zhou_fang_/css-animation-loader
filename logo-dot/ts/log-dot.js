"use strict";
/*
 * @Author: Zhou Fang
 * @Date: 2018-01-22 17:55:26
 * @Last Modified by: Zhou Fang
 * @Last Modified time: 2018-01-23 15:21:54
 */
/**
 * @description add logo dot loading animation
 * @class LogoDotAnimation
 * @parameter  parentContainer: HTMLElement
 * @method  show(); hide(); destroy();
 */
var LogoDotAnimation = /** @class */ (function () {
    /**
     * Creates an instance of LogoDotAnimation.
     * @param {HTMLElement} parentContainer
     * @memberof LogoDotAnimation
     */
    function LogoDotAnimation(parentContainer) {
        this.parentContainer = parentContainer;
        /**
         * @description
         * @type {string}
         * @memberof LogoDotAnimation
         */
        this.template = "\n  \n    <div class=\"spinner \">\n        <div class=\"ball \">\n            <?xml version=\"1.0 \" encoding=\"utf-8 \"?>\n            <!-- Generator: Adobe Illustrator 19.2.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\n            <svg version=\"1.1 \"\n                 fill=\"#03A9F4 \"\n                 xmlns=\"http://www.w3.org/2000/svg \"\n                 xmlns:xlink=\"http://www.w3.org/1999/xlink\n    \"\n                 x=\"0px \"\n                 y=\"0px \"\n                 viewBox=\"0 0 50 50 \"\n                 style=\"enable-background:new 0 0 50 50; \"\n                 xml:space=\"preserve \">\n                <g>\n                    <path d=\"M35.2,13.8v22.1h-7.8v-3.4c-1.8,2.6-4.1,3.8-6.9,3.8c-3.8,0-5.7-1.8-5.7-5.5V14.3c2.6-0.1,5.2-0.3,7.8-0.6v16.7 c0,1.5,0.4,2.2,1.3,2.2c2.3,0,3.5-2.5,3.5-7.5V14.3C30,14.2,32.6,14,35.2,13.8z \" />\n                </g>\n            </svg>\n        </div>\n        <div>LOADING</div>\n    </div>\n\n    ";
        this.css = ".l-d-l-con {\n      position: relative;\n      height: 100%;\n      width: 100%;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-pack: center;\n          justify-content: center;\n      -ms-flex-align: center;\n          align-items: center;\n      text-align: center;\n    }\n    .l-d-l-con .spinner {\n      height: 3.5rem;\n      width: 9rem;\n      text-align: center;\n      background-color: rgba(0, 0, 0, 0.5);\n      padding: 1rem 1.5rem;\n      border-radius: 1rem;\n    }\n    .l-d-l-con .spinner .ball {\n      width: 1.25rem;\n      height: 1.25rem;\n      background-color: #fff;\n      border-radius: 50%;\n      display: inline-block;\n      animation: motion 3s cubic-bezier(0.77, 0, 0.175, 1) infinite;\n    }\n    .l-d-l-con .spinner div {\n      color: rgba(255, 255, 255, 0.8);\n      margin-top: 0.625rem;\n      font-family: sans-serif;\n      letter-spacing: 0.2rem;\n      font-size: 0.725rem;\n    }\n    @keyframes motion {\n      0% {\n        transform: translateX(0) scale(1);\n        opacity: 1;\n      }\n      25% {\n        transform: translateX(-50px) scale(0.6);\n        opacity: 0.2;\n      }\n      50% {\n        transform: translateX(0) scale(1);\n        opacity: 1;\n      }\n      75% {\n        transform: translateX(50px) scale(0.6);\n        opacity: 0.2;\n      }\n      100% {\n        transform: translateX(0) scale(1);\n        opacity: 1;\n      }\n    }     \n    ";
        this.head = document.head || document.getElementsByTagName('head')[0];
        if (!parentContainer || !(parentContainer instanceof HTMLElement))
            throw new Error('Container div error.');
        // append the styles ---
        var cssNode = document.createTextNode(this.css);
        this.styleNode = document.createElement('style');
        this.styleNode.type = 'text/css';
        if (this.styleNode.sheet && this.styleNode.sheet.cssText) {
            this.styleNode.sheet.cssText = cssNode.nodeValue; //ie only
        }
        else {
            this.styleNode.appendChild(cssNode);
        }
        this.head.appendChild(this.styleNode);
        // append html-----------
        this.htmlNode = document.createElement('div');
        this.htmlNode.className = 'l-d-l-con';
        this.htmlNode.innerHTML = this.template;
        parentContainer.appendChild(this.htmlNode);
    }
    /**
     * @description hide the loading animation html node
     * @memberof LogoDotAnimation
     */
    LogoDotAnimation.prototype.hide = function () {
        if (this.htmlNode && this.htmlNode instanceof HTMLElement) {
            this.htmlNode.style.display = 'None';
        }
        else {
            throw new Error('loading element not exit');
        }
    };
    /**
     * @description show the loading animation html node.
     * @memberof LogoDotAnimation
     */
    LogoDotAnimation.prototype.show = function () {
        if (this.htmlNode && this.htmlNode instanceof HTMLElement) {
            this.htmlNode.style.display = '';
        }
        else {
            throw new Error('loading style element not exit');
        }
    };
    /**
     * @description remove the loading animation html node and the stylesheet node
     * @memberof LogoDotAnimation
     */
    LogoDotAnimation.prototype.destroy = function () {
        if (this.htmlNode && this.htmlNode instanceof HTMLElement) {
            this.htmlNode.remove();
        }
        if (this.styleNode && this.styleNode instanceof HTMLStyleElement) {
            this.styleNode.remove();
        }
    };
    return LogoDotAnimation;
}());
//# sourceMappingURL=log-dot.js.map